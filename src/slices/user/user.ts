import { actions } from './user.slice'
const allActions = {
    ...actions
};
export { allActions as actions };
export { reducer } from './user.slice';
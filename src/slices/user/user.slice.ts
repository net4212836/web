import { createSlice } from '@reduxjs/toolkit';
import { UserSlice } from '../../libs/types/user.type';

const initialState: UserSlice = {
    user: null,
    history: []
};

const { reducer, actions, name } = createSlice({
    initialState,
    name: 'user',
    reducers: {
        setUser: (state, action) => {
            console.log(state.user)
            state.user = action.payload
            console.log(state.user)
        },
        setHistory: (state, action) => {
            console.log(state.history)
            state.history.push(action.payload);
            console.log(state.history)
        },
        removeHistory: (state, action) => {
            console.log(state.history)
            state.history = state.history.filter((item) => item.key !== action.payload);
            console.log(state.history)
        }
    },
});

export { actions, name, reducer };
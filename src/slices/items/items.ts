import { actions } from './items.slice';

const allActions = {
    ...actions,
};

export { allActions as actions };
export { reducer } from './items.slice';
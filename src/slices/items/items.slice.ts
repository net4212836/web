import { createSlice } from '@reduxjs/toolkit';
import { ItemsSlice } from '../../libs/types/items.type';
import { getAllData } from './common/mocked-data';

const initialState: ItemsSlice = {
    items: getAllData(),
    selectedItems: [],
    filter: '',
    comments: [{
        id: self.crypto.randomUUID(),
        itemId: 383799534,
        advantages: 'asdasd',
        disadvantages: 'asdasdsa',
        starNumber: 3,
        comment: 'asdasdasdas',
        email: 'asdasdas',
        fullName: 'asdasdsa'
    }]
};

const { reducer, actions, name } = createSlice({
    initialState,
    name: 'items',
    reducers: {
        setFilter: (state, action) => {
            state.filter = action.payload;
        },
        setSelectedItems: (state, action) => {
            console.log(state.selectedItems)
            state.items = state.items.map((item) => item.id !== action.payload.id ? item : { ...item, isSelected: true })
            state.selectedItems?.unshift(action.payload);
            console.log(state.selectedItems)
        },
        removeSelectedItem: (state, action) => {
            console.log(state.selectedItems)
            state.items = state.items.map((item) => item.id !== action.payload.id ? item : { ...item, isSelected: false })
            state.selectedItems = state.selectedItems.filter((item) => item.id !== action.payload.id);
            console.log(state.selectedItems)
        },
        setComment: (state, action) => {
            console.log(state.comments)
            state.comments.unshift(action.payload);
            console.log(state.comments)
        },
        removeComment: (state, action) => {
            console.log(state.comments)
            state.comments = state.comments.filter((item) => item.id !== action.payload);
            console.log(state.comments)
        },
        removeItem: (state, action) => {
            console.log(state.items)
            state.items = state.items.filter((item) => item.id !== action.payload);
            console.log(state.items)
        },
        addItem: (state, action) => {
            console.log(state.items)
            state.items.push(action.payload)
            console.log(state.items)
        }
    },
});

export { actions, name, reducer };
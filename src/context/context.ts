import { createContext } from "react";

export const TokenContext = createContext<{ token: string; setToken: React.Dispatch<React.SetStateAction<string>> } | null>(null)

export const UserNameContext = createContext<string | null>(null)
import { useState } from 'react';

const useStars = () => {
    const [stars, setStars] = useState([
        { id: 1, filled: false },
        { id: 2, filled: false },
        { id: 3, filled: false },
        { id: 4, filled: false },
        { id: 5, filled: false },
    ]);

    const handleSetStarsValue = (starValue: number) => {
        setStars((previous) =>
            previous.map((item) =>
                item.id <= starValue
                    ? { ...item, filled: true }
                    : { ...item, filled: false }
            )
        );
    }

    return { stars, handleSetStarsValue }
}

export { useStars }
import { useMemo } from "react"
import { Items } from '../libs/types/items.type';

const useSearch = (query: string, items: Items[]) => {
    const searchedItems = useMemo(() => {
        const re = new RegExp(query.toLowerCase());
        return items.filter((item) => item.name.toLowerCase().search(re) !== -1)
    }, [items, query]);
    return searchedItems;
}

export { useSearch }
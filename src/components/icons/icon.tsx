import { iconNameToIcon } from '../../libs/maps/icon-name-to-icon';
import { IconName } from '../../libs/types/icon-name.type';

type Properties = {
  name: IconName;
  className?: string;
};

const Icon: React.FC<Properties> = ({ name, className }) => {
  const SelectedIcon = iconNameToIcon[name];

  return <SelectedIcon className={className} />;
};

export { Icon };

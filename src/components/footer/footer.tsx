import styles from './styles.module.scss';

const Footer: React.FC = () => {
  return <div className={styles['container']}>© 2023: Yevhenii Kitan</div>;
};

export { Footer };

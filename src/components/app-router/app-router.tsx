import { FC, useEffect } from 'react';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import { MAIN } from '../../libs/utils/constants';
import { Layout } from '../layout/layout';
import MainPage from '../../pages/main/main-page';
import { ExpandedItem } from '../../pages/expanded-item/expanded-item';
import { ItemComments } from '../../pages/item-comments/item-comments';
import { WashingMachinesPage } from '../../pages/washing-machines/washing-machines';
import { GraphicCardsPage } from '../../pages/graphics-cards/graphic-cards';
import { PlayStationsPage } from '../../pages/play-stations/play-stations';
import { actions as userActions } from '../../slices/user/user';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { History } from '../../pages/history/history';
import { AdminPanel } from '../../pages/admin-panel/admin-panel';

const AppRouter: FC = () => {
  const dispatch = useAppDispatch();
  const location = useLocation();
  useEffect(() => {
    dispatch(
      userActions.setHistory({
        key: Date.now(),
        date: new Date().toString(),
        location: location.pathname,
      })
    );
  }, [dispatch, location.pathname]);

  return (
    <Routes>
      <Route path='/' element={<Layout />}>
        <Route index element={<MainPage />} />
        <Route path='/:id' element={<ExpandedItem />} />
        <Route path='*' element={<Navigate to={MAIN} replace />} />
        <Route path='/:id/comments' element={<ItemComments />} />
        <Route path='/washingMachines' element={<WashingMachinesPage />} />
        <Route path='/graphicsCards' element={<GraphicCardsPage />} />
        <Route path='/playStations' element={<PlayStationsPage />} />
        <Route path='/history' element={<History />} />
        <Route path='/admin' element={<AdminPanel />} />
      </Route>
    </Routes>
  );
};

export default AppRouter;

import styles from './styles.module.scss';
import { Catalog, Search, SelectedItem } from './components/components';
import { Icon } from '../icons/icon';
import logo from '../../assets/img/logo.png';
import smallLogo from '../../assets/img/small-logo.png';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { useRef, useState } from 'react';
import { Dropdown, type MenuProps } from 'antd';
import { Link } from 'react-router-dom';
import { actions as userActions } from '../../slices/user/user';
import { CSSTransition } from 'react-transition-group';
import { LoginForm } from './components/login-form/login-form';

const Header: React.FC = () => {
  const dispatch = useAppDispatch();
  const nodeRef = useRef(null);
  const { selectedItems, user } = useAppSelector((state) => {
    return { selectedItems: state.items.selectedItems, user: state.user.user };
  });

  const [loginShow, setLoginShow] = useState(false);

  const [open, setOpen] = useState(false);
  const showModal = () => {
    setOpen(true);
  };

  const handleLogout = () => {
    dispatch(userActions.setUser(null));
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const items: MenuProps['items'] = [
    {
      key: '1',
      label: (
        <div>{user ? <>Welcome {user.firstName}</> : <>Please log in</>}</div>
      ),
    },
    {
      key: '2',
      label: (
        <button
          className={styles['logout']}
          onClick={user ? handleLogout : () => setLoginShow(true)}
        >
          {user ? <>Logout</> : <>Login</>}
        </button>
      ),
    },
  ];

  return (
    <div className={styles['container']}>
      <div className={styles['layout']}>
        <div className={styles['hamburger-icon']}>
          <Icon name='hamburger' />
        </div>
        <Link to={'/'} className={styles['big-logo']}>
          <img src={logo} style={{ height: '100%' }} />
        </Link>
        <Link to={'/'} className={styles['small-logo']}>
          <img src={smallLogo} style={{ height: '100%' }} />
        </Link>
        <Catalog />
        <Search />
        <Dropdown menu={{ items }} placement='bottom' arrow>
          <button className={styles['user-button']}>
            <Icon name='user' />
          </button>
        </Dropdown>
        <button className={styles['cart-button']} onClick={showModal}>
          <span className={styles['num-items']}>{selectedItems.length}</span>
          <Icon name='cart' />
        </button>
        <CSSTransition
          in={open}
          nodeRef={nodeRef}
          timeout={300}
          classNames={'dialog'}
          unmountOnExit
        >
          <dialog open ref={nodeRef} className={styles['dialog']}>
            <h2>Selected goods</h2>
            {selectedItems.map((item) => (
              <SelectedItem item={item} key={item.id} />
            ))}
            <button className={styles['close']} onClick={handleCancel}>
              Close
            </button>
          </dialog>
        </CSSTransition>
        <LoginForm setLoginShow={setLoginShow} loginShow={loginShow} />
      </div>
    </div>
  );
};

export { Header };

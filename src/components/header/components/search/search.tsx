import { useAppDispatch } from '../../../../hooks/reduxHooks';
import { Icon } from '../../../icons/icon';
import styles from './styles.module.scss';
import { useState, useEffect } from 'react';
import { actions as itemsActions } from '../../../../slices/items/items';

const Search: React.FC = () => {
  const dispatch = useAppDispatch();
  const [filter, setFilter] = useState('');
  const handleFilterReset = () => {
    setFilter('');
    dispatch(itemsActions.setFilter(''));
  };

  useEffect(() => {
    dispatch(itemsActions.setFilter(filter));
  }, [dispatch, filter]);

  return (
    <div className={styles['search-container']}>
      <input
        className={styles['search']}
        value={filter}
        onChange={(event) => setFilter(event.target.value)}
      />
      <button onClick={handleFilterReset} className={styles['reset']}>
        <Icon name={'xMark'} className={styles['x-mark']} />
      </button>
    </div>
  );
};

export { Search };

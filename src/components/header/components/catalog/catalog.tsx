import React from 'react';
import styles from './styles.module.scss';
import { Icon } from '../../../icons/icon';
import { Dropdown, type MenuProps } from 'antd';
import { Link } from 'react-router-dom';

const Catalog: React.FC = () => {
  const items: MenuProps['items'] = [
    {
      key: '1',
      label: <Link to={'/playStations'}>Play stations</Link>,
    },
    {
      key: '2',
      label: <Link to={'/graphicsCards'}>Graphics cards</Link>,
    },
    {
      key: '3',
      label: <Link to={'/washingMachines'}>Washing machines</Link>,
    },
    {
      key: '4',
      label: <Link to={'/history'}>History</Link>,
    },
    {
      key: '5',
      label: <Link to={'/admin'}>Admin panel</Link>,
    },
  ];

  return (
    <Dropdown menu={{ items }} placement='bottom' arrow>
      <button className={styles['container']}>
        <Icon name='catalog' />
        Каталог
      </button>
    </Dropdown>
  );
};

export { Catalog };

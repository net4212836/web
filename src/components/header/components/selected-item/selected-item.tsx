import { useAppDispatch } from '../../../../hooks/reduxHooks';
import { Items } from '../../../../libs/types/items.type';
import styles from './styles.module.scss';
import { actions as itemsActions } from '../../../../slices/items/items';

type Properties = {
  item: Items;
};

const SelectedItem: React.FC<Properties> = ({ item }) => {
  const dispatch = useAppDispatch();

  const handleRemoveSelectedItem = () => {
    dispatch(itemsActions.removeSelectedItem(item));
  };

  return (
    <div className={styles['container']}>
      <div className={styles['information-row']}>
        <img src={item.imageUrl} />
        <div className={styles['information-cell']}>
          <div>{item.name}</div>
          <div className={styles['price']}>{item.price} ₴</div>
        </div>
      </div>
      <div
        className={styles['button-wrapper']}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <button
          className={styles['cancel-button']}
          onClick={handleRemoveSelectedItem}
        >
          Remove
        </button>
      </div>
    </div>
  );
};

export { SelectedItem };

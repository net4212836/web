import React, { useState } from 'react';
import { useAppDispatch } from '../../../../hooks/reduxHooks';
import { actions as userActions } from '../../../../slices/user/user';
import { Button, Modal, Input } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './styles.module.scss';
import { Formik, Form } from 'formik';
import { LoginSchema } from '../../../../libs/validation-schema.ts/login-schema';

type Properties = {
  setLoginShow: (value: React.SetStateAction<boolean>) => void;
  loginShow: boolean;
};

const LoginForm: React.FC<Properties> = ({ setLoginShow, loginShow }) => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);

  return (
    <Modal
      onCancel={() => setLoginShow(false)}
      open={loginShow}
      footer={[
        <Button key='back' onClick={() => setLoginShow(false)}>
          Return
        </Button>,
      ]}
    >
      <Formik
        initialValues={{ email: '', firstName: '', password: '', lastName: '' }}
        onSubmit={(values) => {
          setLoading(true);
          setTimeout(() => {
            dispatch(userActions.setUser(values));
            setLoginShow(false);
            setLoading(false);
          }, 500);
        }}
        validationSchema={LoginSchema}
      >
        {(props) => (
          <Form className={styles['loginForm']}>
            <Input
              placeholder='Email'
              name='email'
              value={props.values.email}
              onChange={props.handleChange}
            />
            {props.errors.email && props.touched.email && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.email}
              </div>
            )}
            <Input
              placeholder='First name'
              value={props.values.firstName}
              name='firstName'
              prefix={<UserOutlined />}
              onChange={props.handleChange}
            />
            {props.errors.firstName && props.touched.firstName && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.firstName}
              </div>
            )}
            <Input
              placeholder='Last name'
              value={props.values.lastName}
              name='lastName'
              prefix={<UserOutlined />}
              onChange={props.handleChange}
            />
            {props.errors.lastName && props.touched.lastName && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.lastName}
              </div>
            )}
            <Input.Password
              placeholder='input password'
              value={props.values.password}
              name='password'
              visibilityToggle={{
                visible: passwordVisible,
                onVisibleChange: setPasswordVisible,
              }}
              onChange={props.handleChange}
            />
            {props.errors.password && props.touched.password && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.password}
              </div>
            )}
            <Button
              htmlType='submit'
              key='submit'
              type='primary'
              loading={loading}
            >
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export { LoginForm };

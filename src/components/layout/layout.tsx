import React from 'react';
import { Footer, Header } from '../components';
import { Outlet } from 'react-router-dom';

import styles from './styles.module.scss';

const Layout: React.FC = () => {
  return (
    <div className={styles['container']}>
      <Header />
      <main className={styles['main']}>
        <Outlet />
      </main>
      <Footer />
    </div>
  );
};

export { Layout };

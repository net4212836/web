/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useField } from 'formik';
import React from 'react';
import styles from './styles.module.scss';

const FormikInput: React.FC<any> = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);
  return (
    <>
      <input {...field} {...props} className={styles['input']} />

      {meta.touched && meta.error ? (
        <div style={{ color: 'red', fontSize: '16px' }}>{meta.error}</div>
      ) : null}
    </>
  );
};

export { FormikInput };

import styles from './styles.module.scss';
import { Icon } from '../icons/icon';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { actions as itemsActions } from '../../slices/items/items';
import { Items } from '../../libs/types/items.type';
import { useNavigate } from 'react-router-dom';

type Properties = {
  item: Items;
};

const Card: React.FC<Properties> = ({ item }) => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const handleSelectItem = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.stopPropagation();
    if (!item.isSelected) {
      dispatch(itemsActions.setSelectedItems(item));
    }
  };

  const handleSelectExpandedItem = () => {
    navigate(`/${item.id}`);
  };

  return (
    <div className={styles['container']} onClick={handleSelectExpandedItem}>
      <div className={styles['image-placeholder']}>
        <img src={item.imageUrl} />
      </div>
      <div className={styles['name']}>{item.name}</div>
      <div className={styles['price']}>
        <span>{item.price} ₴</span>
        <button className={styles['buy']} onClick={handleSelectItem}>
          <Icon name='buyCart' />
          {item.isSelected && (
            <Icon name='selected' className={styles['selected']} />
          )}
        </button>
      </div>
      <div className={styles['ready']}>
        <span className={styles['status']}>Готовий до відправлення</span>{' '}
        <Icon name='truck' />
      </div>
    </div>
  );
};

export { Card };

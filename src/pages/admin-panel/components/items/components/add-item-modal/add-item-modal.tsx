import React, { useState } from 'react';
import { Button, Modal, Input } from 'antd';
import styles from './styles.module.scss';
import { actions as itemActions } from '../../../../../../slices/items/items';
import { Formik, Form, Field } from 'formik';
import { useAppDispatch } from '../../../../../../hooks/reduxHooks';
import { AddItemSchema } from '../../../../../../libs/validation-schema.ts/add-item-schema';
import { FormikInput } from '../../../../../../components/formik-input/formik-input';

type Properties = {
  setLoginShow: (value: React.SetStateAction<boolean>) => void;
  loginShow: boolean;
};

const AddItemModal: React.FC<Properties> = ({ setLoginShow, loginShow }) => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);

  return (
    <Modal
      onCancel={() => setLoginShow(false)}
      open={loginShow}
      footer={[
        <Button key='back' onClick={() => setLoginShow(false)}>
          Return
        </Button>,
      ]}
    >
      <Formik
        validationSchema={AddItemSchema}
        initialValues={{
          id: Math.floor(Math.random() * 1000000000) + 1,
          name: '',
          price: '',
          bigImageUrl: '',
          imageUrl: '',
          isSelected: false,
          category: '',
        }}
        onSubmit={(values, actions) => {
          setLoading(true);
          setTimeout(() => {
            console.log(values);
            dispatch(itemActions.addItem(values));
            setLoginShow(false);
            actions.setSubmitting(false);
            actions.resetForm({
              values: {
                id: Math.floor(Math.random() * 1000000000) + 1,
                name: '',
                price: '',
                bigImageUrl: '',
                imageUrl: '',
                isSelected: false,
                category: '',
              },
            });
            setLoading(false);
          }, 500);
        }}
      >
        {(props) => (
          <Form className={styles['addItemForm']}>
            <Input
              placeholder='Item name'
              name='name'
              value={props.values.name}
              onChange={props.handleChange}
            />
            {props.errors.name && props.touched.name && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.name}
              </div>
            )}
            {/* <Input
              placeholder='Price'
              name='price'
              type='number'
              value={props.values.price}
              onChange={props.handleChange}
            />
            {props.errors.price && props.touched.price && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.price}
              </div>
            )} */}
            <FormikInput placeholder='Price' name='price' type='number' />
            <Input
              placeholder='Big image'
              name='bigImageUrl'
              value={props.values.bigImageUrl}
              onChange={props.handleChange}
            />
            {props.errors.bigImageUrl && props.touched.bigImageUrl && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.bigImageUrl}
              </div>
            )}
            <Input
              placeholder='Image'
              name='imageUrl'
              value={props.values.imageUrl}
              onChange={props.handleChange}
            />
            {props.errors.imageUrl && props.touched.imageUrl && (
              <div style={{ color: 'red', fontSize: '16px' }}>
                {props.errors.imageUrl}
              </div>
            )}
            <div role='group' aria-labelledby='my-radio-group'>
              <label>
                <Field type='radio' name='category' value='washingMachines' />
                Washing machines
              </label>
              <label>
                <Field type='radio' name='category' value='playStation' />
                Play station
              </label>
              <label>
                <Field type='radio' name='category' value='graphicCards' />
                Graphic cards
              </label>
            </div>
            <Button
              htmlType='submit'
              key='submit'
              type='primary'
              loading={loading}
            >
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export { AddItemModal };

/* eslint-disable @typescript-eslint/no-explicit-any */
import { useAppDispatch, useAppSelector } from '../../../../hooks/reduxHooks';
import { Items as ItemsType } from '../../../../libs/types/items.type';
import { actions as itemActions } from '../../../../slices/items/items';
import { Table, Button } from 'antd';
import { AddItemModal } from './components/add-item-modal/add-item-modal';
import { useState } from 'react';

const Items: React.FC = () => {
  const items = useAppSelector((state) => state.items.items);
  const dispatch = useAppDispatch();
  const columns = [
    {
      title: 'Item name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'x',
      render: (_: any, record: ItemsType) => {
        return (
          <a onClick={() => dispatch(itemActions.removeItem(record.id))}>
            Delete
          </a>
        );
      },
    },
  ];
  const [loginShow, setLoginShow] = useState(false);
  return (
    <>
      <AddItemModal loginShow={loginShow} setLoginShow={setLoginShow} />
      <div style={{ padding: '10px 0' }}>
        <Button type='primary' onClick={() => setLoginShow(true)}>
          Add item
        </Button>
      </div>

      <Table dataSource={items} columns={columns} />
    </>
  );
};

export { Items };

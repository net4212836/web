/* eslint-disable @typescript-eslint/no-explicit-any */
import { useAppDispatch, useAppSelector } from '../../../../hooks/reduxHooks';
import { Comment } from '../../../../libs/types/items.type';
import { actions as itemActions } from '../../../../slices/items/items';
import { Table } from 'antd';

const Comments: React.FC = () => {
  const comments = useAppSelector((state) => state.items.comments);
  const dispatch = useAppDispatch();
  const columns = [
    {
      title: 'Item id',
      dataIndex: 'itemId',
      key: 'itemId',
    },
    {
      title: 'Advantages',
      dataIndex: 'advantages',
      key: 'advantages',
    },
    {
      title: 'Disadvantages',
      dataIndex: 'disadvantages',
      key: 'disadvantages',
    },
    {
      title: 'Comment',
      dataIndex: 'comment',
      key: 'comment',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'x',
      render: (_: any, record: Comment) => {
        return (
          <a onClick={() => dispatch(itemActions.removeComment(record.id))}>
            Delete
          </a>
        );
      },
    },
  ];
  return <Table dataSource={comments} columns={columns} />;
};

export { Comments };

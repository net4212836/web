/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';
import { useAppDispatch, useAppSelector } from '../../../../hooks/reduxHooks';
import { Table } from 'antd';
import { type History as HistoryType } from '../../../../libs/types/user.type';
import { actions as userActions } from '../../../../slices/user/user';

const History: React.FC = () => {
  const history = useAppSelector((state) => state.user.history);
  const dispatch = useAppDispatch();
  const columns = [
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
    },
    {
      title: 'Action',
      dataIndex: '',
      key: 'x',
      render: (_: any, record: HistoryType) => {
        return (
          <a onClick={() => dispatch(userActions.removeHistory(record.key))}>
            Delete
          </a>
        );
      },
    },
  ];
  return <Table dataSource={history} columns={columns} />;
};

export { History };

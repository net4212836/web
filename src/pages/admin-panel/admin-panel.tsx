import React from 'react';
import { Tabs, TabsProps } from 'antd';
import { History } from './components/history/history';
import { Comments } from './components/comments/comments';
import { Items } from './components/items/items';

const AdminPanel: React.FC = () => {
  const items: TabsProps['items'] = [
    {
      key: '1',
      label: 'Comments',
      children: <Comments />,
    },
    {
      key: '2',
      label: 'Items',
      children: <Items />,
    },
    {
      key: '3',
      label: 'History',
      children: <History />,
    },
  ];
  return (
    <Tabs
      defaultActiveKey='1'
      items={items}
      centered
      style={{ width: '100%' }}
    />
  );
};

export { AdminPanel };

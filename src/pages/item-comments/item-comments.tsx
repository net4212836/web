import { NavLink, useParams } from 'react-router-dom';
import styles from './styles.module.scss';
import { useState } from 'react';
import { useAppSelector } from '../../hooks/reduxHooks';
import { Items } from '../../libs/types/items.type';
import { AddCommentModal } from './components/components';
import { Comment } from './components/comments/comments';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const ItemComments: React.FC = () => {
  const { id } = useParams();
  const { items, comments } = useAppSelector((state) => {
    return { items: state.items.items, comments: state.items.comments };
  });
  const expandedItem = items.find((item) => item.id === Number(id)) as Items;
  const itemComments = comments.filter(
    (comment) => comment.itemId === Number(id)
  );
  const [open, setOpen] = useState(false);
  const showModal = () => {
    setOpen(true);
  };
  const handleCancel = () => {
    setOpen(false);
  };
  console.log(itemComments);
  return (
    <div className={styles['container']}>
      <div className={styles['navigation-container']}>
        <NavLink className={styles['nav-item']} to={`/${id}`}>
          Усе про товар
        </NavLink>
        <NavLink className={styles['selected-nav-item']} to={`/${id}/comments`}>
          Відгуки
        </NavLink>
      </div>
      <AddCommentModal
        open={open}
        handleCancel={handleCancel}
        id={Number(id)}
      />
      <div className={styles['item-name']}>Відгуки про {expandedItem.name}</div>
      <div className={styles['comments-container']}>
        <div>Залиште свій відгук про цей товар</div>
        <button className={styles['add-comment']} onClick={showModal}>
          Написати відгук
        </button>
      </div>
      <div>
        <TransitionGroup>
          {itemComments.map((comment) => (
            <CSSTransition key={comment.id} timeout={500} classNames='comment'>
              <Comment
                advantages={comment.advantages}
                disadvantages={comment.disadvantages}
                starNumber={comment.starNumber}
                comment={comment.comment}
                fullName={comment.fullName}
              />
            </CSSTransition>
          ))}
        </TransitionGroup>
      </div>
    </div>
  );
};

export { ItemComments };

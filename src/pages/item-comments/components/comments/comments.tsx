/* eslint-disable react-hooks/exhaustive-deps */
import { Comment as CommentType } from '../../../../libs/types/items.type';
import { Icon } from '../../../../components/icons/icon';
import styles from './styles.module.scss';
import { useStars } from '../../../../hooks/hooks';
import { useEffect } from 'react';

const Comment: React.FC<Partial<CommentType>> = ({
  advantages,
  disadvantages,
  starNumber,
  comment,
  fullName,
}) => {
  const { stars, handleSetStarsValue } = useStars();
  useEffect(() => {
    handleSetStarsValue(Number(starNumber));
  }, [starNumber]);

  const starsArray = stars.map((item) =>
    item.id <= (starNumber as number) ? { ...item, filled: true } : item
  );

  return (
    <div className={styles['container']}>
      <div>
        <b>{fullName}</b>
      </div>
      <div>
        {starsArray.map((star) => (
          <Icon key={star.id} name={star.filled ? 'filledStar' : 'star'} />
        ))}
      </div>
      <div>{comment}</div>
      <div>
        <b>Переваги</b>: {advantages}
      </div>
      <div>
        <b>Недоліки</b>: {disadvantages}
      </div>
    </div>
  );
};

export { Comment };

import { Modal } from 'antd';
import { Icon } from '../../../../components/icons/icon';
import styles from './styles.module.scss';
import { actions as itemsActions } from '../../../../slices/items/items';
import { useAppDispatch } from '../../../../hooks/reduxHooks';
import { notification } from 'antd';
import { useStars } from '../../../../hooks/hooks';
import { Formik, Form } from 'formik';
import { useState } from 'react';
import { CommentSchema } from '../../../../libs/validation-schema.ts/add-comment-schema';

type Properties = {
  id: number;
  handleCancel: () => void;
  open: boolean;
};

const AddCommentModal: React.FC<Properties> = ({ id, handleCancel, open }) => {
  const [api, contextHolder] = notification.useNotification();

  type NotificationType = 'success';
  const openNotificationWithIcon = (type: NotificationType) => {
    api[type]({
      message: 'Comment was created',
    });
  };
  const [starNumber, setStarNumber] = useState(0);
  const dispatch = useAppDispatch();
  const { stars, handleSetStarsValue } = useStars();

  const handleSetStars = (
    id: number,
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.preventDefault();
    event.stopPropagation();
    handleSetStarsValue(id);
    setStarNumber(id);
  };

  return (
    <Modal
      open={open}
      title='Написати відгук'
      footer={[]}
      onCancel={handleCancel}
    >
      {contextHolder}
      <Formik
        initialValues={{
          id: self.crypto.randomUUID(),
          itemId: id,
          advantages: '',
          disadvantages: '',
          comment: '',
          email: '',
          fullName: '',
        }}
        validationSchema={CommentSchema}
        onSubmit={(values, actions) => {
          dispatch(itemsActions.setComment({ ...values, starNumber }));
          openNotificationWithIcon('success');
          handleSetStarsValue(0);
          actions.setSubmitting(false);
          actions.resetForm({
            values: {
              id: self.crypto.randomUUID(),
              itemId: id,
              advantages: '',
              disadvantages: '',
              comment: '',
              email: '',
              fullName: '',
            },
          });
          setStarNumber(0);
          handleCancel();
        }}
      >
        {(props) => (
          <Form className={styles['container']}>
            <div className={styles['star-container']}>
              {stars.map((star) => (
                <button
                  type='button'
                  key={star.id}
                  className={styles['star-button']}
                  onClick={(event) => {
                    handleSetStars(star.id, event);
                  }}
                >
                  <Icon name={star.filled ? 'filledStar' : 'star'} />
                </button>
              ))}
            </div>
            <div className={styles['input-container']}>
              <label htmlFor='advantages'>Переваги</label>
              <input
                type='text'
                name='advantages'
                className={styles['input-field']}
                value={props.values.advantages}
                onChange={props.handleChange}
              />
              {props.errors.advantages && props.touched.advantages && (
                <div style={{ color: 'red', fontSize: '16px' }}>
                  {props.errors.advantages}
                </div>
              )}
              <label htmlFor='disadvantages'>Недоліки</label>
              <input
                type='text'
                name='disadvantages'
                className={styles['input-field']}
                value={props.values.disadvantages}
                onChange={props.handleChange}
              />
              {props.errors.disadvantages && props.touched.disadvantages && (
                <div style={{ color: 'red', fontSize: '16px' }}>
                  {props.errors.disadvantages}
                </div>
              )}
              <label htmlFor='comment'>Коментар</label>
              <textarea
                name='comment'
                className={styles['input-field']}
                rows={8}
                value={props.values.comment}
                onChange={props.handleChange}
              />
              {props.errors.comment && props.touched.comment && (
                <div style={{ color: 'red', fontSize: '16px' }}>
                  {props.errors.comment}
                </div>
              )}
              <label htmlFor='name'>Ваше ім'я та прізвище</label>
              <input
                type='text'
                name='fullName'
                className={styles['input-field']}
                value={props.values.fullName}
                onChange={props.handleChange}
              />
              {props.errors.fullName && props.touched.fullName && (
                <div style={{ color: 'red', fontSize: '16px' }}>
                  {props.errors.fullName}
                </div>
              )}
              <label htmlFor='email'>Електронна пошта</label>
              <input
                type='text'
                name='email'
                className={styles['input-field']}
                value={props.values.email}
                onChange={props.handleChange}
              />
              {props.errors.email && props.touched.email && (
                <div style={{ color: 'red', fontSize: '16px' }}>
                  {props.errors.email}
                </div>
              )}
            </div>
            <button type='submit' className={styles['submit-button']}>
              Залишити відгук
            </button>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export { AddCommentModal };

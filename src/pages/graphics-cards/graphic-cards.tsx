import { Card } from '../../components/components';
import { useAppSelector } from '../../hooks/reduxHooks';
import { useSearch } from '../../hooks/useSearch';
import styles from './styles.module.scss';

const GraphicCardsPage: React.FC = () => {
  const { filter, items } = useAppSelector((state) => {
    const items = state.items.items.filter(
      (item) => item.category === 'graphicCards'
    );
    return { items, filter: state.items.filter };
  });
  const filteredItems = useSearch(filter, items);

  return (
    <div className={styles['container']}>
      {filteredItems.map((item) => (
        <Card item={item} key={item.id} />
      ))}
    </div>
  );
};

export { GraphicCardsPage };

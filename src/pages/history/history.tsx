import React from 'react';
import { useAppSelector } from '../../hooks/reduxHooks';
import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import styles from './styles.module.scss';
import { History as HistoryType } from '../../libs/types/user.type';

const History: React.FC = () => {
  const history = useAppSelector((state) => state.user.history);

  const columns: ColumnsType<HistoryType> = [
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
    },
  ];

  console.log(history);
  return (
    <div className={styles['container']}>
      <Table columns={columns} dataSource={history} />
    </div>
  );
};

export { History };

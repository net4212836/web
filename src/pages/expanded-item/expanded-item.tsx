import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import styles from './styles.module.scss';
import smallLogo from '../../assets/img/small-logo.png';
import { Icon } from '../../components/icons/icon';
import { actions as itemsActions } from '../../slices/items/items';
import { NavLink, useParams } from 'react-router-dom';
import { useState } from 'react';
import { Items } from '../../libs/types/items.type';

const ExpandedItem: React.FC = () => {
  const [currency, setCurrency] = useState('UAH');
  const { id } = useParams();
  const items = useAppSelector((state) => {
    return state.items.items;
  });
  const expandedItem = items.find((item) => item.id === Number(id)) as Items;
  const dispatch = useAppDispatch();
  const handleSelectItem = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.stopPropagation();
    if (!expandedItem?.isSelected) {
      dispatch(itemsActions.setSelectedItems(expandedItem));
    }
  };

  return (
    <div className={styles['container']}>
      <div className={styles['navigation-container']}>
        <NavLink className={styles['selected-nav-item']} to={`/${id}`}>
          Усе про товар
        </NavLink>
        <NavLink className={styles['nav-item']} to={`/${id}/comments`}>
          Відгуки
        </NavLink>
      </div>
      <div className={styles['item-info']}>
        <div className={styles['image']}>
          <img src={expandedItem?.bigImageUrl} />
        </div>
        <div className={styles['text-info']}>
          <div className={styles['item-name']}> {expandedItem?.name}</div>
          <div className={styles['item-buy-info']}>
            <div className={styles['seller']}>
              Продавець:
              <img src={smallLogo} />
              PowerSocket
            </div>
            <div className={styles['item-price']}>
              Ціна:{' '}
              {currency === 'UAH'
                ? expandedItem?.price
                : (expandedItem?.price / 36.49).toFixed(2)}
              {currency === 'UAH' ? '₴' : '$'}
            </div>
            <button
              onClick={handleSelectItem}
              className={
                expandedItem.isSelected
                  ? styles['buy-button-selected']
                  : styles['buy-button']
              }
            >
              Купити
            </button>
            Валюта:
            <div className={styles['currency']}>
              <button
                className={
                  currency === 'UAH'
                    ? styles['selected']
                    : styles['non-selected']
                }
                onClick={() => setCurrency('UAH')}
              >
                ₴
              </button>
              <div className={styles['separator']}></div>
              <button
                className={
                  currency === 'UAH'
                    ? styles['non-selected']
                    : styles['selected']
                }
                onClick={() => setCurrency('USD')}
              >
                $
              </button>
            </div>
            <div className={styles['ready']}>
              <span className={styles['status']}>Готовий до відправлення</span>
              <Icon name='truck' />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export { ExpandedItem };

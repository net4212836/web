import { useState } from 'react';
import AppRouter from './components/app-router/app-router';
import { BrowserRouter } from 'react-router-dom';
import { TokenContext, UserNameContext } from './context/context';
import './style.css';

function App() {
  const [token, setToken] = useState('hfsadjfhgerwhwegrh213sadhjfds');
  const userName = 'Yevhenii';

  return (
    <>
      <TokenContext.Provider value={{ token, setToken }}>
        <UserNameContext.Provider value={userName}>
          <BrowserRouter>
            <AppRouter />
          </BrowserRouter>
        </UserNameContext.Provider>
      </TokenContext.Provider>
    </>
  );
}

export default App;

import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { reducer as userReducer } from "../slices/user/user";
import { reducer as itemsReducer } from "../slices/items/items";

export const rootReducer = combineReducers({
    user: userReducer,
    items: itemsReducer,
});

export const setupStore = () => {
    return configureStore({
        reducer: rootReducer
    });
}

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];

type User = {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

type History = {
    key: string;
    date: string;
    location: string;
}

type UserSlice = {
    user: null | User;
    history: History[];
}

export { type UserSlice, type History, type User }
type IconName = 'hamburger' | 'xMark' | 'catalog' | 'user' | 'cart' | 'truck' | 'buyCart' | 'selected' | 'filledStar' | 'star';

export { type IconName };

export { type IconName } from './icon-name.type';
export { type UserSlice } from './user.type';
export { type ItemsSlice, type Items, type Comment } from './items.type';
export { type Product } from './mocked-data.type';
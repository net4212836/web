/* eslint-disable @typescript-eslint/no-explicit-any */
type Product = {
    id: number;
    href: string;
    group: {
        id: number;
        is_group_primary: number;
        href: string | null;
    };
    category: {
        id: number;
        title: string;
        href: string;
        use_group_links: boolean;
        root_id: number;
        isSuperMarket: boolean;
    };
    title: string;
    brand: {
        id: number;
        title: string;
        name: string;
        logo: string | null;
    };
    comments: {
        amount: number;
        mark: number;
    };
    docket: string;
    images: Array<{
        original: string;
        big_tile: string;
        preview: string;
    }>;
    loyalty: {
        pl_bonus_charge_pcs: number;
        pl_use_instant_bonus: number;
    };
    price: {
        old: number;
        current: number;
        pcs: string;
        min_month_price: string;
    };
    status: string;
    status_inherited: string;
    sell_status: string;
    merchant_id: number;
    seller_id: number;
    state: string;
    tag: null | unknown;
    premium_program: boolean;
    sla_id: number;
};

type WashingMachine =
    {
        id: number;
        title: string;
        title_suffix: string | null;
        price: number;
        old_price: number;
        price_pcs: string;
        min_month_price: string;
        href: string;
        status: string;
        status_inherited: string;
        sell_status: string;
        category_id: number;
        seller_id: number;
        merchant_id: number;
        brand: string;
        brand_id: number;
        group_id: number;
        group_name: string | null;
        group_title: string | null;
        pl_bonus_charge_pcs: number;
        pl_use_instant_bonus: number;
        state: string;
        docket: {
            option_id: number;
            option_name: string;
            option_title: string;
            value_id: string | number;
            value_title: string;
            value_name: string;
            order: number;
            goods_id: number;
            href: string | null;
            values?: any;
        }[] | null;
        mpath: string;
        is_group_primary: number;
        dispatch: number;
        premium: number;
        preorder: number;
        comments_amount: number;
        comments_mark: number;
        gift: any | null; // You can specify a more specific type if necessary
        tag: any | null; // You can specify a more specific type if necessary
        pictograms: any | null; // You can specify a more specific type if necessary
        title_mode: string | null;
        use_group_links: any | null; // You can specify a more specific type if necessary
        is_need_street_id: boolean;
        image_main: string;
        images: {
            main: string;
            preview: string;
            hover: string;
            all_images: string[];
        };
        parent_category_id: number;
        is_docket: boolean;
        primary_good_title: string | null;
        groups: any[] | {
            prices: {
                count: number;
                min: string;
                max: string;
            }
        }; // You can specify a more specific type if necessary
        stars: string;
        discount: number;
        config: {
            title: boolean;
            brand: boolean;
            image: boolean;
            price: boolean;
            old_price: boolean;
            promo_price: boolean;
            status: boolean;
            bonus: boolean;
            gift: boolean;
            rating: boolean;
            wishlist_button: boolean;
            compare_button: boolean;
            buy_button: boolean;
            tags: boolean;
            pictograms: boolean;
            description: boolean;
            variables: boolean;
            hide_rating_review: boolean;
        };
    };
type PlayStation = {
    id: number;
    title: string;
    title_suffix: string | null;
    price: number;
    old_price: number;
    price_pcs: string;
    min_month_price: string;
    href: string;
    status: string;
    status_inherited: string;
    sell_status: string;
    category_id: number;
    seller_id: number;
    merchant_id: number;
    brand: string;
    brand_id: number;
    group_id: number | null;
    group_name: string | null;
    group_title: string | null;
    pl_bonus_charge_pcs: number;
    pl_use_instant_bonus: number;
    state: string;
    docket: {
        option_id: number;
        option_name: string;
        option_title: string;
        value_id: string;
        value_title: string;
        value_name: string;
        order: number;
        goods_id: number;
        href: string | null;
    }[] | null;
    mpath: string;
    is_group_primary: number;
    dispatch: number;
    premium: number;
    preorder: number;
    comments_amount: number;
    comments_mark: number;
    gift: any | null; // You can specify a more specific type if necessary
    tag: {
        name: string;
        title: string;
        priority: number;
        goods_id: number;
    } | null;
    pictograms: {
        is_auction: boolean;
        view_position: number;
        order: number;
        id: number;
        goods_id: number;
        title: string;
        image_url: string;
        view_type: string;
        announce: any | null;
        has_description: number;
        description: any | null;
        url: any | null;
        url_title: any | null;
        icon_mobile: string;
    }[] | null;
    title_mode: string | null;
    use_group_links: any | null; // You can specify a more specific type if necessary
    is_need_street_id: boolean;
    image_main: string;
    images: {
        main: string;
        preview: string;
        hover: string;
        all_images: string[];
    };
    parent_category_id: number;
    is_docket: boolean;
    primary_good_title: string | null;
    groups: any[] | {
        prices: {
            count: number;
            min: string;
            max: string;
        }
    }; // You can specify a more specific type if necessary
    stars: string;
    discount: number;
    config: {
        title: boolean;
        brand: boolean;
        image: boolean;
        price: boolean;
        old_price: boolean;
        promo_price: boolean;
        status: boolean;
        bonus: boolean;
        gift: boolean;
        rating: boolean;
        wishlist_button: boolean;
        compare_button: boolean;
        buy_button: boolean;
        tags: boolean;
        pictograms: boolean;
        description: boolean;
        variables: boolean;
        hide_rating_review: boolean;
    };
};

export { type Product, type PlayStation, type WashingMachine }
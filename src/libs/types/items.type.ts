type Items = {
    id: number;
    name: string;
    price: number;
    bigImageUrl: string;
    imageUrl: string;
    isSelected: boolean;
    category: string
}

type Comment = {
    id: string;
    itemId: number;
    advantages: string;
    disadvantages: string;
    starNumber: number;
    comment: string;
    email: string;
    fullName: string;
}

type ItemsSlice = {
    items: Items[];
    selectedItems: Items[];
    filter: string;
    comments: Comment[];
}

export { type ItemsSlice, type Items, type Comment }
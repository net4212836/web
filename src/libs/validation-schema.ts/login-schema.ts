import * as Yup from 'yup';

export const LoginSchema = Yup.object().shape({
    firstName: Yup.string().min(3, 'Too Short!').required('Required'),
    lastName: Yup.string().max(40, 'Too Long!').required('Required'),
    password: Yup.string()
        .matches(/[A-Z]/, 'Password must contain at least one uppercase letter')
        .matches(
            /[0-9]/,
            'Password must contain at least one non-letter character'
        )
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
});
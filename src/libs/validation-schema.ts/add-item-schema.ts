import * as Yup from 'yup';

export const AddItemSchema = Yup.object().shape({
    name: Yup.string().required('Required'),
    price: Yup.number().required('Required').max(100000, 'Must be bigger smaller 100000').positive('Must be positive'),
    bigImageUrl: Yup.string().required('Required'),
    imageUrl: Yup.string().required('Required'),
    category: Yup.string().required('Required'),
});
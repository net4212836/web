import * as Yup from 'yup';

export const CommentSchema = Yup.object().shape({
    fullName: Yup.string().min(3, 'Too Short!').required('Required'),
    advantages: Yup.string().min(10, 'Too Short!').required('Required'),
    disadvantages: Yup.string().min(10, 'Too Short!').required('Required'),
    comment: Yup.string().min(10, 'Too Short!').required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
});
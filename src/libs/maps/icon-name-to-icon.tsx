import { ReactComponent as Hamburger } from '../../assets/icons/bars.svg';
import { ReactComponent as CatalogIcon } from '../../assets/icons/catalog.svg';
import { ReactComponent as XMark } from '../../assets/icons/x-mark.svg';
import { ReactComponent as Cart } from '../../assets/icons/cart.svg';
import { ReactComponent as User } from '../../assets/icons/user.svg';
import { ReactComponent as Truck } from '../../assets/icons/truck.svg';
import { ReactComponent as Selected } from '../../assets/icons/select-circle.svg';
import { ReactComponent as BuyCart } from '../../assets/icons/buy-cart.svg';
import { ReactComponent as Star } from '../../assets/icons/star.svg';
import { ReactComponent as FilledStart } from '../../assets/icons/filled-start.svg';

import { IconName } from '../types/icon-name.type';

const iconNameToIcon: Record<
  IconName,
  React.FunctionComponent<React.SVGProps<SVGSVGElement>>
> = {
  hamburger: Hamburger,
  xMark: XMark,
  catalog: CatalogIcon,
  cart: Cart,
  user: User,
  truck: Truck,
  buyCart: BuyCart,
  selected: Selected,
  star: Star,
  filledStar: FilledStart,
};

export { iconNameToIcon };
